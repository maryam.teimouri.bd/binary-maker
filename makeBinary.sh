#/!bin/bash

#### Written by: Maryam Teimouri - maryamt78.23@gmail.com on 2018-12-28

signature='
$$\   $$\ $$\   $$\  $$$$$$\   $$$$$$\
$$ | $$  |$$$\  $$ |$$  __$$\ $$  __$$\
$$ |$$  / $$$$\ $$ |\__/  $$ |$$ /  \__|
$$$$$  /  $$ $$\$$ | $$$$$$  |$$ |
$$  $$<   $$ \$$$$ |$$  ____/ $$ |
$$ |\$$\  $$ |\$$$ |$$ |      $$ |  $$\
$$ | \$$\ $$ | \$$ |$$$$$$$$\ \$$$$$$  |
\__|  \__|\__|  \__|\________| \______/


----------------------------------------
          ***HELIOS_based***
----------------------------------------
by :
    a.sadreddin
    a.sadraii
    m.moazen
    s.nazari
    a.tabasi
    m.teimouri
    +
----------------------------------------
'


main () {

    echo "enter destination path"
    read dPath
    while [ ! -d $dPath ] ; do
        echo "directory doesnt exist. enter another"
        read dPath
    done


    echo "enter Agent directory"
    read agentPath
    while [ ! -d $agentPath ] ; do
        echo "directory doesnt exist. enter another"
        read agentPath
    done


    echo "enter Lib directory"
    read libPath
    while [ ! -d $libPath ] ; do
        echo "directory doesnt exist. enter another"
        read libPath
    done

    echo "enter Team Name"
    read teamName
    while [ -z $teamName ] ; do
        echo "the string is empty enter another"
        read teamName
    done

    echo "enter coach Name Name"
    read coachName
    while [ -z $coachName ] ; do
        echo "the string is empty enter another"
        read coachName
    done

    
    tempName=`make_tempFile`

    make_agent

    make_lib

    make_confs

    make_samplePlayer

    make_sampleCoach

    make_formations

    make_start

    "make_start1"

    "make_start2"

    make_startAll

    make_killFile

    cd ~/$tempName

    tar -czvf ""$teamName"-BIN.tar.gz" $dPath 1>/dev/null 2>/dev/null
    
    cd $dPath

    mkdir dist    

    mv ~/$tempName/""$teamName"-BIN.tar.gz" $dPath/dist

    rm -r ~/$tempName

}


make_tempFile () {

    cd ~
    tempName="temp-`date +%Y-%m-%d`"

    while [ -d $tempName ] ; do
        echo "the file by default name already exists :$tempName"
        echo "enter a name"
        read tempName
    done


    mkdir $tempName
    echo "$tempName"

}


make_agent () {

    cp -r $agentPath ~/$tempName/Agent

    cd ~/$tempName/Agent
    ./configure --with-librcsc="`pwd`/Lib/" 1>/dev/null 2>/dev/null
    `make -j8` 1>/dev/null 2>/dev/null

}


make_lib () {

    cp -r $libPath ~/$tempName/Lib

    cd ~/$tempName/Lib
    ./configure --prefix="`pwd|sed s'/...$//'`/Agent/Lib" 1>/dev/null
    `make -j8` 1>/dev/null 2>/dev/null
    `make install` 1>/dev/null 2>/dev/null

    cd ~/$tempName/Agent/Lib/lib
    lib_files=`ls lib*`
    mkdir $dPath/Lib
    for i in $lib_files ; do
        cp $i $dPath/Lib
    done

}


make_confs () {

    coachConf="# coach agent configuration file

team_name : $teamName
version : 14

coach_name : $coachName
use_coach_name : off

interval_msec : 50
server_wait_seconds : 5

host : localhost
port : 6002

use_eye : on
hear_say : on

#use_hetero : on
#use_advice : on
#use_freeform : on

#use_team_graphic : off
#max_team_graphic_per_cycle : 32
"

echo "$coachConf" > "$dPath/coach.conf"

    playerConf="# player agent configuration file

team_name : $teamName
version : 14

server_wait_seconds : 5
normal_view_time_thr : 20
synch_see : on

host : localhost
port : 6000

use_communication : on
hear_opponent_audio : off

#debug
log_dir : /tmp

#debug_server_connect
#debug_server_logging
#debug_server_host : localhost
#debug_server_port : 6032

#offline_logging
#offline_log_ext : .ocl

debug_log_ext : .log
"

echo "$playerConf" > "$dPath/player.conf"

}


make_samplePlayer () {

    cd ~/$tempName/Agent

    sample_player=`find -name sample_player`

    if [ ! -z $sample_player ] ; then
        cp -r $sample_player $dPath
        echo "sample_player is copied"
    else
        echo"couldnt copy sample_player"
        while [ -z $sample_player ]; do
            echo "enter sample_player directory"
            read samplePlayer
            sample_player=`find -name $samplePlayer`
        done
        cp -r $sample_player $dPath
        echo "sample_player is copied"
    fi

    cd $dPath

    chmod +x "sample_player"

    mv sample_player ""$teamName"_player"

}


make_sampleCoach () {

    cd ~/$tempName/Agent

    sample_coach=`find -name sample_coach`

    if [ ! -z $sample_coach ] ; then
        cp -r $sample_coach $dPath
        echo "sample_coach is copied"
    else
        echo "couldnt copy sample_coach"
        while [ -z $sample_coach ]; do
            echo "enter sample_coach directory"
            read sampleCoach
            sample_coach=`find -name $sampleCoach`
        done
        cp -r $sample_coach $dPath
        echo "sample_coach is copied"
    fi

    cd $dPath

    chmod +x "sample_coach"

    mv sample_coach ""$teamName"_coach"

}


make_formations () {

    cd ~/$tempName/Agent

    formations=`find -name formations`

    if [ ! -z $formations ] ; then
        cp -r $formations $dPath/Formations
        echo "formations are copied"
    else 
        echo "couldnt copy formations"
        while [ -z $formations ]; do
            echo "enter formation directory"
            read formations_name
            formations=`find -name $formations_name`
        done
        cp -r $formations $dPath/Formations
        echo "formations are copied"
    fi

}

make_start () {

    start_file="#!/bin/sh
HOST=\$1
BASEDIR=\$2
NUM=\$3
DIR=\`dirname \$0\`

LIBPATH=\"\${DIR}/Lib\"
if [ x\"\$LIBPATH\" != x ]; then
    if [ x\"\$LD_LIBRARY_PATH\" = x ]; then
        LD_LIBRARY_PATH=\$LIBPATH
    else
        LD_LIBRARY_PATH=\$LIBPATH:\$LD_LIBRARY_PATH
    fi
    export LD_LIBRARY_PATH
fi

teamname=\"$teamName\"

player=\"\${DIR}/"$teamName"_player\"
coach=\"\${DIR}/"$teamName"_coach\"

config=\"\${DIR}/player.conf\"
config_dir=\"\${DIR}/Formations\"
coach_config=\"\${DIR}/coach.conf\"

opt=\"--player-config \${config} --config_dir \${config_dir}\"
opt=\"\${opt} -h \${HOST} -t \${teamname}\"

coachopt=\"--coach-config \${coach_config}\"
coachopt=\"\${coachopt} -h \${HOST} -t \${teamname}\"

cd \$BASEDIR

case \$NUM in
    1)
        \$player \$opt -g
        ;;
    12)
        \$coach \$coachopt
        ;;
    *)
        \$player \$opt
        ;;
esac
"

cd $dPath
echo "$start_file" > "start"
chmod +x "start"

}


make_start1 () {

start1_file="#!/bin/sh
echo '
$signature
'
DIR=\`dirname \$0\`
\${DIR}/start 127.0.0.1 . 1 &
sleep 1;
\${DIR}/start 127.0.0.1 . 2 &
\${DIR}/start 127.0.0.1 . 3 &
\${DIR}/start 127.0.0.1 . 4 &
\${DIR}/start 127.0.0.1 . 5 &
\${DIR}/start 127.0.0.1 . 6 &
"
    cd $dPath
    echo "$start1_file" > "start1"
    chmod +x "start1"


}


make_start2 () {

    start2_file="#!/bin/sh
echo '
$signature
'
DIR=\`dirname \$0\`
\${DIR}/start 127.0.0.1 . 7 &
\${DIR}/start 127.0.0.1 . 8 &
\${DIR}/start 127.0.0.1 . 9 &
\${DIR}/start 127.0.0.1 . 10 &
\${DIR}/start 127.0.0.1 . 11 &
\${DIR}/start 127.0.0.1 . 12 &
"
    cd $dPath
    echo "$start2_file" > "start2"
    chmod +x "start2"


}


make_startAll () {

    startAll_file="#!/bin/sh
DIR=\`dirname \$0\`
\${DIR}/start1
\${DIR}/start2
"
    cd $dPath
    echo "$startAll_file" > "startAll"
    chmod +x "startAll"

}

make_killFile () {

    killFile="#!/bin/sh
kill -SIGKILL \`pidof rcssserver\`&&
kill -SIGKILL \`pidof "$teamName"_player\`&&
kill -SIGKILL \`pidof "$teamName"_coach\`
"

    cd $dPath
    echo "$killFile" > "kill"
    chmod +x "kill"

}

main
